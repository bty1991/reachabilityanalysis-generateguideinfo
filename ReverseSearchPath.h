#include "programCFG.h"
#include <vector> 

using namespace std;


//用于存储路径集合的数据结构
class ReverseSinglePath{//一条路径
public:	
	vector<CFGNode*> path;
	int length;
	void PathAddNode(CFGNode* node){
		path.push_back(node);
	}
};


void SearchReversePaths(BasicBlock* entry,BasicBlock* exit,BasicBlock* 	mainEntry,Module &m);
int SuccNumber(CFGNode* node);
CFGNode* ConvertBBToNode(BasicBlock* block);
CFGNode* index_Succ(int index,CFGNode* node);


#include "ReverseSearchPath.h"
#include <vector>
using namespace llvm;


std::vector<ReverseSinglePath*> PathSet;
std::vector<ReverseSinglePath*> AdjustedMainToEntryPathSet;
std::vector<ReverseSinglePath*> AdjustedEntryToExitPathSet;
std::vector<ReverseSinglePath*> CompletePathSet;

std::map<BasicBlock*, bool> *visited;
extern std::map<pair<CFGNode*,CFGNode*>,std::vector<CFGNode*> >* FortifyPath;


//根据基本块找到控制流图的节点
CFGNode* ConvertBBToNode(BasicBlock* block){
        return &(*(ProgramCFG::nodes))[block];
}

//获取所有路径的集合
void getReversePathSet(ReverseSinglePath* singlePath,int length){
	ReverseSinglePath* rpath=new ReverseSinglePath();
	for(unsigned int i=0;i<singlePath->path.size();i++){
		rpath->PathAddNode(singlePath->path.at(i));
		errs() <<singlePath->path.at(i)<<"->";
	}
	errs() <<length<<'\n';
	rpath->length=length;
	PathSet.push_back(rpath);	
	
}

//根据路径生成制导信息
void getGuideInfo(){
	for(unsigned int i=0;i<CompletePathSet.size();i++){
		for(unsigned int j=0;j<CompletePathSet.at(i)->path.size();j++){
			BasicBlock* bb=CompletePathSet.at(i)->path.at(j)->bb;	
			BasicBlock::iterator i = bb->begin();
			int number=getLineNumber(i);
			errs() <<getFilename(i)<<"  "<<number<<"->";
		}
		errs() <<'\n';
	}
						
}

//根据index寻找对应的cfg节点
CFGNode* index_Prev(int index,CFGNode* node){
	int number=0;
	if(index==0)	return node->firstPrev->v;
	else{
		Prev* p=node->firstPrev;
		while(p!=NULL){
			if(number==index) return p->v;
			p=p->nextPrev;
			number++;			
		}
	}
	return NULL;
}

//CFG中前驱节点的数目
int PrevNumber(CFGNode* node){
	int number=0;
	if(node->firstPrev==NULL) return number;
	else{
		Prev* p=node->firstPrev;
		while(p!=NULL){
			number++;
			p=p->nextPrev;
			
		}
		return number;
	}
}
//------------------------------------------------
//根据index寻找对应的cfg节点
CFGNode* index_Succ(int index,CFGNode* node){
	int number=0;
	if(index==0)	return node->firstSucc->v;
	else{
		Succ* p=node->firstSucc;
		while(p!=NULL){
			if(number==index) return p->v;
			p=p->nextSucc;
			number++;
			
		}
	}
	return NULL;
}

//CFG中后继节点的数目
int SuccNumber(CFGNode* node){
	int number=0;
	if(node->firstSucc==NULL) return number;
	else{
		Succ* p=node->firstSucc;
		while(p!=NULL){
			number++;
			p=p->nextSucc;
			
		}
		return number;
	}
}

//CFG中某两个节点是否直接连通可达
bool IsConnective(CFGNode* sNode,CFGNode* dNode){ 
	if(sNode->firstSucc==NULL) {  return false;}
	else if(dNode == sNode->firstSucc->v) {  return true;}
	else{ 
		Succ* cNode=sNode->firstSucc;
		while(cNode!=NULL){
			
			if(cNode->v==dNode) { return true;}
			cNode=cNode->nextSucc;
		}
		return false;
	}
}




// 深度优先搜索
// path用来记录路径
// visited 用来标记搜索过的节点，初始化全部为false
// v 当前的节点
// des 目的节点
// length 目前已经得到的路径的长度

void SearchPaths(CFGNode* v, CFGNode* des, int length,std::map<BasicBlock*, bool> *visit,ReverseSinglePath* singlePath) { 
    if ((*visit)[v->bb]) return;
    singlePath->PathAddNode(v);
    if (v == des) {
	getReversePathSet(singlePath, length);
	return;
    } else {
        (*visit)[v->bb]= true;
        /*for (int i = 0; i < nodeV->size(); i++) 
            if ( IsConnective(v,nodeV->at(i))&& (!(*visit)[nodeV->at(i)->bb])) {                
                SearchPaths(nodeV->at(i), des, length+1,visit,singlePath);
		singlePath->path.pop_back();
            }*/
	/*for(int i=0;i<SuccNumber(v);i++){		
		if(!(*visit)[index_Succ(i,v)->bb]){
			SearchPaths(index_Succ(i,v), des, length+1,visit,singlePath);
			singlePath->path.pop_back();
		}
		
	}*/
	for(int i=0;i<PrevNumber(v);i++){		
		if(!(*visit)[index_Prev(i,v)->bb]){
			SearchPaths(index_Prev(i,v), des, length+1,visit,singlePath);
			singlePath->path.pop_back();
		}
		
	}
	(*visit)[v->bb]= false;
	
    }
}

bool ContainNodeList(ReverseSinglePath* rpath,std::vector<CFGNode*> nodelist){
	int ContainTag=0;
	for(unsigned int i=0;i<nodelist.size();i++){
		for(unsigned int j=0;j<rpath->path.size();j++){
			if(nodelist.at(i)==rpath->path.at(j))
				{ContainTag++; break;}
		}
	}
	if(ContainTag==nodelist.size()) return true;
	else return false;
		
}

void ChooseOptimalPath(int mode,CFGNode* Dpoint,CFGNode* Lpoint){
	if(mode==1){
		int OptimalLength=PathSet.at(0)->length;
		for(unsigned int i=0;i<PathSet.size();i++){
			if(OptimalLength>PathSet.at(i)->length)
				OptimalLength=PathSet.at(i)->length;
		}
		for(unsigned int i=0;i<PathSet.size();i++){
			if(OptimalLength==PathSet.at(i)->length)
				AdjustedMainToEntryPathSet.push_back(PathSet.at(i));
				
		}		
		
	}else if(mode==2){
		std::map<pair<CFGNode*,CFGNode*>,std::vector<CFGNode*> >::iterator it;
		it=FortifyPath->find(make_pair(Dpoint,Lpoint));
		
		if( it == FortifyPath->end() )
			errs() <<" Error: Can not find Use Point"<<'\n';
		for(unsigned int i=0;i<PathSet.size();i++){
			if(ContainNodeList(PathSet.at(i),it->second))
				AdjustedEntryToExitPathSet.push_back(PathSet.at(i));
		}
				
	}else
		errs() <<" Error: Choose Optimal Path"<<'\n';
}


void PathSplice(){
	for(unsigned int i=0;i<AdjustedEntryToExitPathSet.size();i++){
		for(unsigned int j=0;j<AdjustedMainToEntryPathSet.size();j++){
			ReverseSinglePath* rpath=AdjustedEntryToExitPathSet.at(i);
			for(unsigned int k=1;k<AdjustedMainToEntryPathSet.at(j)->path.size();k++)
				rpath->PathAddNode(AdjustedMainToEntryPathSet.at(j)->path.at(k));
			CompletePathSet.push_back(rpath);
			
		}
	}
}

void FreeMemory(){
	for(unsigned int i=0;i<PathSet.size();i++)
		delete PathSet.at(i);
}



//主要的执行函数

void SearchReversePaths(BasicBlock* pathentry,BasicBlock* pathexit,BasicBlock* 	mainEntry,Module &m){
	ReverseSinglePath* singlePath=new ReverseSinglePath();
	visited=new std::map<BasicBlock*,bool> ;
	for(Module::iterator f = m.begin(); f != m.end(); f++)
		for(Function::iterator bb = f->begin(); bb != f->end(); bb++){
			visited->insert(make_pair(bb,false));
	}
	CFGNode* pathEntry=ConvertBBToNode(pathentry);
	CFGNode* pathExit=ConvertBBToNode(pathexit);
    	CFGNode* mainentry=ConvertBBToNode(mainEntry);
	if(pathEntry==NULL || pathExit==NULL)
		return;
	else{
		SearchPaths(pathEntry,mainentry,0,visited,singlePath);
		if(PathSet.size()==0) errs() <<" Warning:The target node is not reachable from the starting node"<<'\n';//check reachablity from starting node to definition node
		ChooseOptimalPath(1,pathEntry,pathExit);
		delete singlePath;
		ReverseSinglePath* singlePath=new ReverseSinglePath();	
		PathSet.clear();//reuse the data structure PathSet
		SearchPaths(pathExit,pathEntry,0,visited,singlePath);
		if(PathSet.size()==0) errs() <<" Warning:The target node is not reachable from the starting node"<<'\n';//check reachablity from definition node to leak point
		ChooseOptimalPath(2,pathEntry,pathExit);
		PathSplice();//Splice path
		getGuideInfo();// get gudied information
	}
	FreeMemory();
	delete visited;	
}
	

